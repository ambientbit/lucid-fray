﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("PostProcessing/Blur")]
[ExecuteInEditMode]
public class PostBlur : MonoBehaviour
{
    public Material blurMaterial;

    [Range(0, 10)]
    public int iterations;

    [Range(0, 4)]
    public int scale;

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        int nW = source.width >> scale;
        int nH = source.height >> scale;

        RenderTexture temp = RenderTexture.GetTemporary(nW, nH);
        Graphics.Blit(source, temp);
        for (int i = 0; i < iterations; i++)
        {
            RenderTexture sTemp = RenderTexture.GetTemporary(nW, nH);
            Graphics.Blit(temp, sTemp, blurMaterial);
            RenderTexture.ReleaseTemporary(temp);
            temp = sTemp;
        }
        Graphics.Blit(temp, destination);
        RenderTexture.ReleaseTemporary(temp);
    }
}
