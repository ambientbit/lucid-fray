﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Inventory : MonoBehaviour 
{
	public float ClayInventory;
	public float CoalInventory;
	public float CottonInventory;
	public float LeatherInventory;
	public float WoodInventory;
	public float GunpowderInventory;
	public float CopperInventory;
	public float IronInventory;

	float ClayRequirements;
	float CoalRequirements;
	float CottonRequirements;
	float LeatherRequirements;
	float WoodRequirements;
	float GunpowderRequirements;
	float CopperRequirements;
	float IronRequirements;

	GameObject Clay_Amount;
	GameObject Coal_Amount;
	GameObject Cotton_Amount;
	GameObject Leather_Amount;
	GameObject Wood_Amount;
	GameObject Gunpowder_Amount;
	GameObject Copper_Amount;
	GameObject Iron_Amount;

	GameObject CraftingRecipe;

	// Use this for initialization
	void Start () 
	{
		Clay_Amount = GameObject.Find("ClayAmount");
		Clay_Amount.GetComponent<Text>().text = ClayInventory.ToString();

		Coal_Amount = GameObject.Find("CoalAmount");
		Coal_Amount.GetComponent<Text>().text = CoalInventory.ToString();

		Cotton_Amount = GameObject.Find("CottonAmount");
		Cotton_Amount.GetComponent<Text>().text = CottonInventory.ToString();

		Leather_Amount = GameObject.Find("LeatherAmount");
		Leather_Amount.GetComponent<Text>().text = LeatherInventory.ToString();

		Wood_Amount = GameObject.Find("WoodAmount");
		Wood_Amount.GetComponent<Text>().text = WoodInventory.ToString();

		Gunpowder_Amount = GameObject.Find("GunpowderAmount");
		Gunpowder_Amount.GetComponent<Text>().text = GunpowderInventory.ToString();

		Copper_Amount = GameObject.Find("CopperAmount");
		Copper_Amount.GetComponent<Text>().text = CopperInventory.ToString();

		Iron_Amount = GameObject.Find("IronAmount");
		Iron_Amount.GetComponent<Text>().text = IronInventory.ToString();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void UpdateInventory ()
	{
		ClayInventory -= ClayRequirements;
		CoalInventory -= CoalRequirements;
		CottonInventory -= CottonRequirements;		
		LeatherInventory -= LeatherRequirements;		
		WoodInventory -= WoodRequirements;				
		GunpowderInventory -= GunpowderRequirements;					
		CopperInventory -= CopperRequirements;
		IronInventory -= IronRequirements;

		Clay_Amount.GetComponent<Text>().text = ClayInventory.ToString();
		Coal_Amount.GetComponent<Text>().text = CoalInventory.ToString();
		Cotton_Amount.GetComponent<Text>().text = CottonInventory.ToString();
		Leather_Amount.GetComponent<Text>().text = LeatherInventory.ToString();
		Wood_Amount.GetComponent<Text>().text = WoodInventory.ToString();
		Gunpowder_Amount.GetComponent<Text>().text = GunpowderInventory.ToString();
		Copper_Amount.GetComponent<Text>().text = CopperInventory.ToString();
		Iron_Amount.GetComponent<Text>().text = IronInventory.ToString();
	}

	public void CraftFootSoldier ()
	{
		CraftingRecipe = GetComponent<RecipeManager>().Recipes[0];

		ClayRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Clay;
		CoalRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Coal;
		CottonRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Cotton;
		LeatherRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Leather;
		WoodRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Wood;
		GunpowderRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Gunpowder;
		CopperRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Copper;
		IronRequirements = CraftingRecipe.GetComponent<CraftingMaterials>().Iron;

		if(ClayRequirements <= ClayInventory)
		{
			if(CoalRequirements <= CoalInventory)
			{
				if(CottonRequirements <= CottonInventory)
				{
					if(LeatherRequirements <= LeatherInventory)
					{
						if(WoodRequirements <= WoodInventory)
						{
							if(GunpowderRequirements <= GunpowderInventory)
							{
								if(CopperRequirements <= CopperInventory)
								{
									if(IronRequirements <= IronInventory)
									{
										Debug.Log("Craft FootSoldier Card");
										UpdateInventory();
									}
									else
									{
										Debug.Log("Not Enough Iron");
									}
								}
								else
								{
									Debug.Log("Not Enough Copper");
								}
							}
							else
							{
								Debug.Log("Not Enough Gunpowder");
							}
						}
						else
						{
							Debug.Log("Not Enough Wood");
						}
					}
					else
					{
						Debug.Log("Not Enough Leather");
					}
				}
				else
				{
					Debug.Log("Not Enough Coton");
				}
			}
			else
			{
				Debug.Log("Not Enough Coal");
			}
		}
		else
		{
			Debug.Log("Not enough Clay");
		}
	}
}
