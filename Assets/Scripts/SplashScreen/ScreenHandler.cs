﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScreenHandler : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(loadMainMenu());
    }

    IEnumerator loadMainMenu()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(1);
    }
}