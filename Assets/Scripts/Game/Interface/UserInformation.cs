﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[AddComponentMenu("Game/Info/UserInformation")]
public class UserInformation : MonoBehaviour
{
    public int userId;

    private string m_username;
    private int m_health;

    [SerializeField] private Image m_image;
    [SerializeField] private Text m_userLabel;
    [SerializeField] private Text m_healthLabel;

    public void Start()
    {

    }
}
