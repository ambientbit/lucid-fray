﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[AddComponentMenu("Game/GameHandler")]
public class GameHandler : MonoBehaviour
{
    #region Register events
    // register delegates
    public delegate void StateHandler();
    public delegate void TurnHandler();

    // register events
    public static event StateHandler GameStarted;
    public static event StateHandler GameEnded;

    public static event TurnHandler TurnStarted;
    public static event TurnHandler TurnEnded;

    // raise events
    protected virtual void OnGameStarted()
    {
        if (GameStarted != null)
        {
            GameStarted();
        }
    }

    protected virtual void OnGameEnded()
    {
        if (GameEnded != null)
        {
            GameEnded();
        }
    }

    protected virtual void OnTurnStarted()
    {
        if (TurnStarted != null)
        {
            TurnStarted();
        }
    }

    protected virtual void OnTurnEnded()
    {
        if (TurnEnded != null)
        {
            TurnEnded();
        }
    }
    #endregion

    public UserInformation playerInfo;
    public UserInformation opponentInfo;

    public void OnEnable()
    {
    }

    public void OnDisable()
    {
    }

    public void Start()
    {
        OnGameStarted();
    }

    public void Update()
    {
    }
}
