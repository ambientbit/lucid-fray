﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Steamworks;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject m_userPanel;
    [SerializeField] private Text m_userNameField;
    [SerializeField] private Image m_userProfile;

    public void Start()
    {
        if(SteamManager.Initialized)
        {
            // get the username and stor it in the usernamefield
            string name = SteamFriends.GetPersonaName();
            m_userNameField.text = name;

            // get the avatar and store it in the userprofile field
            CSteamID user = SteamUser.GetSteamID();
            int avatar = SteamFriends.GetMediumFriendAvatar(user);

            uint imageWidth = 0;
            uint imageHeight = 0;
            bool ret = SteamUtils.GetImageSize(avatar, out imageWidth, out imageHeight);
            if(ret)
            {
                if(imageWidth > 0 && imageHeight > 0)
                {
                    byte[] image = new byte[4 * imageWidth * imageHeight * sizeof(char)];
                    ret = SteamUtils.GetImageRGBA(avatar, image, (int)(4 * imageWidth * imageHeight * sizeof(char)));
                    if(ret)
                    {
                        Texture2D profilePic = new Texture2D((int)imageWidth, (int)imageHeight, TextureFormat.RGBA32, false, true);
                        profilePic.LoadRawTextureData(image);
                        profilePic.Apply();
                        // apply the profile pic on the field
                        m_userProfile.sprite = Sprite.Create(profilePic, new Rect(0, 0, profilePic.width, profilePic.height), new Vector2(0.5f, 0.5f));
                    }
                }
            }
        }
        else
        {
            m_userPanel.SetActive(false);
        }
    }

    public void Btn_OptionsPane()
    {
        // show game options
    }

    // Buttons
    public void Btn_ExitGame()
    {
        Application.Quit();
    }
}
